# README #

## GO SNEK! ##
This can be run locally, or see it live [here](https://gosnek.herokuapp.com/)

Repo: [on bitbucket](https://bitbucket.org/luchosmith/arcade-game)

* Sample 2D Arcade Game based on "Snake"
* Written in pure Javascript
* works on mobile

## TODO ## 
* a bit more refactoring
* styles should be updated of course
* it would be cool if there were food for snek to eat... preferably mice!
