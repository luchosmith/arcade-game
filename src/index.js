import * as browser from './browser'
import {KEYS} from './constants'

(function () {

    let snek, canvas, ctx, gameover

    let DIR = {
        left: false,
        right: true,
        up: false,
        down: false
    }

    class Snek {

        constructor(speed = 3, x=10, y=10, width=20, height=20) {
            this.speed = speed
            this.x = x
            this.y = y
            this.width = width
            this.height = height
        }
    
        draw() {
            ctx.fillStyle = gameover ? 'rgb(255,0,0)' : 'rgb(0, 0, 0)'
            ctx.fillRect(this.x, this.y, this.width, this.height)
        }

        move() {
            let imgData 
            
            if (DIR.left || DIR.right ||  DIR.down || DIR.up) {

                // collision detection is tricky
                // for now, handle it based on current direction
                if (DIR.left) {
                    this.x -= this.speed
                    imgData = ctx.getImageData( this.x, this.y, 1, 1)
                    if (imgData.data[3] === 255 || this.x <= 0 ) {
                        gameover = true
                    }
                } else if (DIR.right) {
                    this.x += this.speed
                    imgData = ctx.getImageData( this.x + this.width, this.y, 1, 1)
                    if (imgData.data[3] === 255 || this.x > canvas.width - this.width) {
                        gameover = true
                    }
                } else if (DIR.up) {
                    this.y -= this.speed
                    imgData = ctx.getImageData( this.x, this.y, 1, 1)
                    if (imgData.data[3] === 255 || this.y <= 0) {
                        gameover = true
                    }
                } else if (DIR.down) {
                    this.y += this.speed
                    imgData = ctx.getImageData( this.x, this.y + this.height, 1, 1)
                    if (imgData.data[3] === 255 || this.y > canvas.height - this.height) {
                        gameover = true
                    }
                }
    
                this.draw()

                if (gameover) {
                    callGameOver()
                } 
            }
        }
    
    }

    function callGameOver() {
        alert('Game Over!')
        //window.setTimeout(reset, 3000)
    }

    function unsetDir(dir) {
      for (var d in DIR) {
          DIR[d] = false
      }
    }

    function reset() {
        gameover = false
        unsetDir()
        DIR['right'] = true
        snek = new Snek()
        ctx.clearRect(0, 0, canvas.width, canvas.height)
        animate()
    }
    
    function animate() {
        if(!gameover) {
            requestAnimFrame( animate )
            snek.move()
        }
    }

    function enableTouch() {

        document.addEventListener('touchstart', handleTouchStart, false)
        document.addEventListener('touchmove', handleTouchMove, false)

        var xDown = null
        var yDown = null

        function handleTouchStart(evt) {
            xDown = evt.touches[0].clientX
            yDown = evt.touches[0].clientY
        }

        function handleTouchMove(evt) {
            if (!xDown || !yDown) {
                return
            }

            var xUp = evt.touches[0].clientX
            var yUp = evt.touches[0].clientY

            var xDiff = xDown - xUp
            var yDiff = yDown - yUp

            unsetDir()

            // determine axis, then direction
            if (Math.abs(xDiff) > Math.abs(yDiff)) {
                if (xDiff > 0) {
                    DIR['left'] = true
                } else {
                    DIR['right'] = true
                }
            } else {
                if (yDiff > 0) {
                    DIR['up'] = true
                } else {
                    DIR['down'] = true
                }
            }

            xDown = null
            yDown = null
        }
    }

    function keyEvents() {
        document.onkeydown = (e) => {
          let keyCode = (e.keyCode) ? e.keyCode : e.charCode
          if (KEYS[keyCode]) {
            e.preventDefault()
            unsetDir()
            DIR[KEYS[keyCode]] = true
          }
        }
    }


    function init() {

        if (!browser.isCanvasSupported()) {
            alert('canvas not supported.')
            return
        }

        let canvasHeight, canvasWidth
        const isMobile = browser.isMobile()

        canvas = document.getElementById('snek-canvas')

        if (isMobile) {
            canvasWidth = window.screen.availWidth
            canvasHeight = window.screen.availHeight - 150 //for header
            enableTouch()
        } else {
            canvasWidth = 480
            canvasHeight = 320
        }
        keyEvents()

        canvas.setAttribute('width', canvasWidth)
        canvas.setAttribute('height', canvasHeight)
        ctx = canvas.getContext('2d')

        reset()
        
    }

    if (document.readyState === "complete") {
        init()
    } else {
        document.addEventListener("DOMContentLoaded", init)
    }

})()
